var Qualification = require('../../models/qualification');

module.exports = function(req, res){
    var params = {
        name: req.param('name')
    }

    Qualification.create(params, function(err, qualification){
        if (err){
            console.log('Error in create: '+err);
            return res.redirect('qualification');
        }
        // already exists
        if (qualification) {
            console.log('There is already such a profession '+qualification);
            return req.flash('message','Qualification Already Exists');
        }
        return res.redirect('qualification');
    });
};