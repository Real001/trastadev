var Event = require('../../../models/event');
var Plan_Evnet = require('../../../models/event_plan');
var ValidateModel = require('validate-model');
var validate = ValidateModel.validate;

module.exports = function(req, res) {
    var params = {
        title: req.param('title'),
        description: req.param('description'),
        location : req.param('location'),
        start_date: req.param('start_date'),
        end_date: req.param('end_date'),
        status: req.param('status')
    };

    Event
        .create(params, function(err, event){
            if (err){

            }
            console.log(event);
            var paramsplan = {
                event: event,
                user: req.user._id,
                status: 'Назначено'
            }
            Plan_Evnet
                .create(paramsplan, function (err, plan) {
                    console.log(plan)
                })
            res.redirect('/user/event/my');
        })
}