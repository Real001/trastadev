var Event = require('../../../models/event');

module.exports = function (){
    Event
        .find({'status':'Предстоящее'} )
        .exec(function(err, event){
            if (err){
                console.log('Произошла ошибка при поиске мероприятий');
                return;
            }
            if (event!= null) {
                module.exports.event = event;
            }
        });
};