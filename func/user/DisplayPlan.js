var Plan = require('../models/plan');

module.exports = function(req, res){
    Plan
        .find({'user':req.user._id})
        .sort({appointmentDate: 'asc'})
        .populate('course')
        .exec(function(err,plan){
            if (err){
                console.log('error');
            } else{
                module.exports.plan = plan;
            }
        })
}