var News = require('../../models/news');

module.exports = function() {

    News
        .find()
        .exec(function(err, news){
            if (err){
                console.log('Произшла ошибка при загрузке новостей');
                return;
            }
            console.log(news);
            module.exports.news = news;
        });
};