var Speciality = require('../../models/speciality');
module.exports = function(){

    Speciality.find(function(err, speciality){
        if (err){
            console.log('Произошла ошибка при выводе специальностей');
            return;
        }
        module.exports.speciality = speciality;
    })

};