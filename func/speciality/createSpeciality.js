var Speciality = require('../../models/speciality');

module.exports = function(req, res){
    var params = {
        name: req.param('name')
    }

    Speciality.create(params, function(err, speciality){
        if (err){
            console.log('Error in create: '+err);
            return res.redirect('speciality');
        }
        // already exists
        if (speciality) {
            console.log('There is already such a profession '+speciality);
            return req.flash('message','Speciality Already Exists');
        }
        return res.redirect('speciality');
    });
};