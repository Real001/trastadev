var Course = require('../../models/course');
var Qualification = require('../../models/qualification');
var Speciality = require('../../models/speciality');
module.exports = function(req, res){
    var IdSpeciality = req.user.speciality;
    var IdQualification = req.user.qualification;

    Course
        .find({'speciality': IdSpeciality, 'qualification': IdQualification})
        .exec(function(err, course){
            if (err){
                console.log('Произошла ошибка');
            }
            module.exports.course = course;
        })

    Qualification
        .findOne({'_id': IdQualification})
        .exec(function(err, qualification){
            if (err){

            }
            module.exports.qualification = qualification;
        })
    Speciality
        .findOne({'_id': IdSpeciality})
        .exec(function(err, speciality){
            if (err){

            }
            module.exports.speciality = speciality;
        })

}