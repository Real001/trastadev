var Course = require('../../models/course');
var ValidateModel = require('validate-model');
var validate = ValidateModel.validate;

module.exports = function(req, res) {
    var params = {
        title: req.param('title'),
        topic: req.param('topic'),
        speciality: req.param('speciality'),
        qualification: req.param('qualification'),
        material: req.param('material')
    };

    Course
        .create(params, function(err, course){
            if (err){

            }
            console.log(course);
            res.redirect('/course');
        })
}