var Course = require('../../models/course');

module.exports = function (){
    Course
        .find()
        .populate('specialities')
        .populate('qualifications')
        .exec(function(err, course){
        if (err){
            console.log('Произошла ошибка при поиске курсов');
            return;
        }
            if (course!= null) {
                module.exports.course = course;
            }
    });
};