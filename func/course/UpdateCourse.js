var Course = require('../../models/course');

module.exports = function(req, res){
    var couseId = req.url;
    couseId = couseId.substr(15);

    var updateCourse = {
        title: req.param('title'),
        topic: req.param('title'),
        speciality: req.param('speciality'),
        qualification: req.param('qualification'),
        material: req.param('material'),
        test: req.param('test')
    };

    Course
        .update({_id: couseId}, updateCourse)
        .exec(function(err, course){
            if (err){
                req.flash('error','Во время обновления произошла ошибка');
                return res.redirect('/course');
            }
            if(!course){
                req.flash('error','Курс не найден');
                return res.redirect('course');
            }
            res.redirect('/course');
        })
}