var Plan = require('../../models/plan');
var Course = require('../../models/course');

module.exports = function (id){
    Plan
        .findOne({'_id': id})
        .exec(function(err, plan){
            if (err){

            }
            module.exports.plan = plan;

            Course
                .findOne({'_id': plan.course})
                .exec(function(err, course){
                    if (err){

                    }
                    module.exports.course = course;
                })
        })
}