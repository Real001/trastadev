var Admin = require('../../models/admin')

module.exports = function (req, res){
    var adminId = req.user._id;
    var updatedAdmin = {
        lastName: req.param('lastName'),
        firstName: req.param('firstName'),
        middleName: req.param('middleName'),
        username: req.param('username'),
        Post: req.param('Post')
    };
    Admin
        .update({_id: adminId}, updatedAdmin)
        .exec(function (err, admin){
            if (err){
                req.flash('error','Во время обновления произошла ошибка');
                return res.redirect('profile');
            }
            if(!admin){
                req.flash('error','Профиль не найден');
                return res.redirect('profile');
            }
            return res.redirect('users');
        });
};
