var Admin = require('../../models/admin');
var bCrypt = require('bcrypt-nodejs');
var passport = require('passport');

module.exports = function (req, res){
    var adminId = req.user._id;
    var updatepassword = {
        password: createHash(req.param('password'))
    };

    Admin
        .update({_id:adminId}, updatepassword)
        .exec(function(err, admin){
            if (err){
                req.flash('error','Во время обновления произошла ошибка');
                return res.redirect('profile');
            }
            if(!admin){
                req.flash('error','Профиль не найден');
                return res.redirect('profile');
            }
            return res.redirect('users');
        });


};

var createHash = function(password){
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};
