var User = require('../../models/user');

module.exports = function(){
    User.find(function (err, users) {
        var context = {
            users: users.map(function (user) {
                return {
                    lastName: user.lastName,
                    firstName: user.firstName,
                    middleName: user.middleName,
                    post: user.post,
                    speciality: user.speciality,
                    qualification: user.qualification,
                    email: user.username,
                    _id: user._id
                }
            })
        };
        module.exports.context = context;

    })
}
