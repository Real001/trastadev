var Plan = require('../../models/plan');
var Course = require('../../models/course');

module.exports = function(id){
    var userId = id;
    var pcourse =[];

    Plan
        .find({'user': userId})
        .populate('courses')
        .exec(function(err, plan){
            if (err){

            } else {
                module.exports.plan = plan;
                for (var i=0; i<plan.length; i++) {
                    Course
                        .findOne({'_id': plan[i].course})
                        .exec(function (err, course) {
                            if (err) {

                            } else {
                                pcourse[i-1] = course;
                                module.exports.course = pcourse;
                            }
                        })
                }
            }

        })
}