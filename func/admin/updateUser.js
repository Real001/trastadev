var User = require('../../models/user');
var Speciality = require('../../models/speciality');

module.exports = function(req, res){
    var userId =req.url ;
    userId = userId.substr(14);

    var updateUser = {
        lastName: req.param('lastName'),
        firstName: req.param('firstName'),
        middleName: req.param('middleName'),
        post: req.param('post'),
        speciality: req.param('speciality'),
        qualification: req.param('qualification')
    };

    User
        .update({_id:userId}, updateUser)
        .exec(function(err, user){
            if (err){
                req.flash('error','Во время обновления произошла ошибка');
                return res.redirect('users');
            }
            if(!user){
                req.flash('error','Профиль не найден');
                return res.redirect('users');
            }
            res.redirect('/users');
        });
};