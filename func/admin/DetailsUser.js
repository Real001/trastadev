var User = require('../../models/user');

module.exports = function(id) {

    User
        .find({'_id': id}, function (err, user) {
            var context = {
                user: user.map(function (userinfo) {
                    return {
                        lastName: userinfo.lastName,
                        firstName: userinfo.firstName,
                        middleName: userinfo.middleName,
                        post: userinfo.post,
                        speciality: userinfo.speciality,
                        qualification: userinfo.qualification,
                        email: userinfo.username,
                        _id: userinfo._id
                    }
                })
            }
                module.exports.context = context;
        })}