var User = require('../../models/user');
var passport = require('passport');
var bCrypt = require('bcrypt-nodejs');

module.exports = function(req, res){
    var params = {
        username: req.param('username'),
        lastName: req.param('lastName'),
        firstName: req.param('firstName'),
        middleName:req.param('middleName'),
        post: req.param('post'),
        speciality: req.param('speciality'),
        qualification:  req.param('qualification'),
        password: createHash(req.param('password'))
    };
    User.create(params, function(err, user){
        if (err){
            console.log('Error in SignUp: '+err);
            return res.redirect('users/create');
        }
        // already exists
        if (!user) {
            console.log('User already exists with username: '+username);
            return req.flash('message','User Already Exists');
        }
        res.redirect('/users')
    });

};

var createHash = function(password){
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}