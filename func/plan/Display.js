var Plan = require('../../models/plan');
var Course = require('../../models/course');
var Qualification = require('../../models/qualification');
var Speciality = require('../../models/speciality');

module.exports = function(req, res){
    var userId = req.user._id;
    var pcourse =[];
    var pqual = [];
    var pspec = [];

    Plan
        .find({'user': userId})
        .populate('courses')
        .exec(function(err, plan){
            if (err){

            } else {
                module.exports.plan = plan;
                for (var i=0; i<plan.length; i++) {
                    Course
                        .findOne({'_id': plan[i].course})
                        .exec(function (err, course) {
                            if (err) {

                            } else {
                                pcourse[i-1] = course;
                                module.exports.course = pcourse;

                                Qualification
                                    .findOne({'_id': course.qualification})
                                    .exec(function(err, qualification){
                                        pqual[i-1] = qualification;
                                        module.exports.qualification = pqual;
                                    })

                                Speciality
                                    .findOne({'_id': course.speciality})
                                    .exec(function(err, speciality){
                                        pspec[i-1] = speciality;
                                        module.exports.speciality = pspec;
                                    })

                            }
                        })

                }
            }

        })
}