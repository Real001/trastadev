var Plan = require('../../models/plan');
var Course = require('../../models/course');

module.exports = function(req, res){
    var IdUser = req.user._id;
    var titleCourse = req.param('course');
    var deadline = req.param('deadline');

    Course
        .findOne({'title': titleCourse})
        .exec(function(err, course){
            if (err) {
                return res.notFound();
            }
            if(!course) {
                return res.notFound();
            }

            var params  = {
                'deadline': deadline,
                'course': course.id,
                'user': IdUser
            };

            Plan
                .findOne({'course': course.id, 'user': IdUser})
                .exec(function (err, course) {
                    if (course) {
                        req.flash('error', 'Ошибка добавления. Материал уже находится на изучении.');
                        return res.redirect('/user/plan/create/');
                    } else {
                        Plan
                            .create(params,function (err, plan) {
                                if (err){

                                }
                                return res.redirect('/user');
                            });
                    }
                });
        })
}