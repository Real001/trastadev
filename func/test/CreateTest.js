var Test = require('../../models/test');
var DisplayQuestion = require('../test/DisplayQuestion');

module.exports = function(req, res){
    DisplayQuestion();
    var question =[];
    for (var i = 0; i<DisplayQuestion.question.length; i++){
        question[i] = DisplayQuestion.question[i]._id;
    }

    var params = {
        name: req.param('name'),
        question: question
    }

    Test.create(params, function(err, test){
        if (err){
            console.log('Error in create' + err);
            return res.redirect('/material/create');
        }

        if (test){
            console.log('There is already such a profession ' + test);
            return req.flash('message','Test Already Exists');
        }
        return res.redirect('/material/create');
    });

};