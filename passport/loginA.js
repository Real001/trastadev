var LocalStrategy   = require('passport-local').Strategy;
var Admin = require('../models/admin');
var User = require('../models/user');
var bCrypt = require('bcrypt-nodejs');
var passport = require('passport');

module.exports = function(passport){
    passport.use('loginA', new LocalStrategy({
            passReqToCallback : true
        },
        function(req, username, password, done) {
            if (req.url == '/admin') {
                // check in mongo if a user with username exists or not
                Admin.findOne({'username': username},
                    function (err, admin) {
                        // In case of any error, return using the done method
                        if (err)
                            return done(err);
                        // Username does not exist, log the error and redirect back
                        if (!admin) {
                            console.log('User Not Found with username '+username);
                            return done(null, false, req.flash('message', 'User Not found.'));
                        }
                        // User exists but wrong password, log the error
                        if (!isValidPasswordAdmin(admin, password)) {
                            console.log('Invalid Password');
                            return done(null, false, req.flash('message', 'Invalid Password')); // redirect back to login page
                        }
                        // User and password both match, return user from done method
                        // which will be treated like success
                        return done(null, admin);
                    }
                );
            };

            if (req.url == '/'){
                User.findOne({ 'username' :  username },
                    function(err, user) {
                        // In case of any error, return using the done method
                        if (err)
                            return done(err);
                        // Username does not exist, log the error and redirect back
                        if (!user){
                            console.log('User Not Found with username '+username);

                            return done(null, false, req.flash('message', 'User Not found.'));
                        }
                        // User exists but wrong password, log the error
                        if (!isValidPasswordUser(user, password)){
                            console.log('Invalid Password');
                            return done(null, false, req.flash('message', 'Invalid Password')); // redirect back to login page
                        }
                        // User and password both match, return user from done method
                        // which will be treated like success
                        return done(null, user);
                    }
                );
            }

        })
    );


    var isValidPasswordAdmin = function(admin, password){
        return bCrypt.compareSync(password, admin.password);
    };

    var isValidPasswordUser = function(user, password){
        return bCrypt.compareSync(password, user.password);
    };

}