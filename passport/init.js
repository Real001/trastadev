var loginA = require('./loginA');
var signup = require('./signup');
var Admin = require('../models/admin');
var User = require('../models/user');

module.exports = function(passport){

    // Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(admin, done) {
        console.log('serializing admin: ');console.log(admin);
        done(null, admin._id);
    });

    passport.deserializeUser(function(id, done) {

        Admin.findById(id, function(err, admin) {
            if (admin == null){
                User.findById(id, function(err, admin){
                    console.log('deserializing admin:', admin);
                   done(err, admin);
                });
            }else {
                console.log('deserializing admin:', admin);
                done(err, admin);
            }
        });

    });

    // Setting up Passport Strategies for Login and SignUp/Registration
    loginA(passport);
    signup(passport);

}