var LocalStrategy   = require('passport-local').Strategy;
var Admin = require('../models/admin');
var bCrypt = require('bcrypt-nodejs');
var passport = require('passport');

module.exports = function(passport){

    passport.use('signup', new LocalStrategy({
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {

            findOrCreateAdmin = function(){
                // find a user in Mongo with provided username
                Admin.findOne({ 'username' :  username }, function(err, admin) {
                    // In case of any error, return using the done method
                    if (err){
                        console.log('Error in SignUp: '+err);
                        return done(err);
                    }
                    // already exists
                    if (admin) {
                        console.log('User already exists with username: '+username);
                        return done(null, false, req.flash('message','User Already Exists'));
                    } else {
                        // if there is no user with that email
                        // create the user
                        var newAdmin = new Admin();

                        // set the user's local credentials
                        newAdmin.username = username;
                        newAdmin.password = createHash(password);
                        newAdmin.Post = req.param('Post');
                        newAdmin.firstName = req.param('firstName');
                        newAdmin.lastName = req.param('lastName');
                        newAdmin.middleName = req.param('middleName');

                        // save the user
                        newAdmin.save(function(err) {
                            if (err){
                                console.log('Error in Saving user: '+err);
                                throw err;
                            }
                            console.log('User Registration succesful');
                            return done(null, newAdmin);
                        });
                    }
                });
            };
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateAdmin);
        })
    );

    // Generates hash using bCrypt
    var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

}