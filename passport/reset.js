var LocalStrategy   = require('passport-local').Strategy;
var Admin = require('../models/admin');
var bCrypt = require('bcrypt-nodejs');

module.exports = function(passport){
    passport.use('reset', new LocalStrategy({
            passReqToCallback : true
        },
        function randomString() {
            return Math.round((Math.pow(36, 9) - Math.random() * Math.pow(36, 9))).toString(36).slice(1);
        },
        function(req, username, password, done) {
            password = randomString();
            Admin.findOne({ 'username' :  username },
                function(err, admin) {
                    if (err)
                        return done(err);
                    if (!admin){
                        console.log('User Not Found with username '+username);
                        return done(null, false, req.flash('message', 'User Not found.'));
                    }
                    // User exists but wrong password, log the error
                    if (admin)
                        Passport
                            .update({'admin': admin.id},{'password': password})//TODO: Expiry time add
                            .exec(function (err, pas) {
                                console.log(password);
                                EmailService.reset(username, password);
                                req.flash('info', 'Вам отправленно письмо с новым паролем.');
                                //res.redirect('/admin');
                                // User and password both match, return user from done method
                    // which will be treated like success
                    return done(null, admin);
                                })
                }


    );}