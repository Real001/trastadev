var mongoose = require('mongoose');

module.exports = mongoose.model('Material',{
    title:{type: String, required: true},
    topic: {type: String, required: true},
    book: {type: Text, required: true},
    test: {type: mongoose.Schema.Types.ObjectId, ref: 'test', required: true}
});