var mongoose = require('mongoose');

module.exports = mongoose.model('News', {
    Ntitle: {type: String, required: true},
    Ntext: {type: Text, required: true},
    Ndate: {type: Date, required: true},
    Nadmin: {type: String, required: true}
});