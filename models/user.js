
var mongoose = require('mongoose');

module.exports = mongoose.model('User',{
    username: {type: String, required: true},
    password: {type: String},
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    middleName: {type: String, required: true},
    post: {type: String, required: true},
    speciality: {type: String, required: true},
    qualification: {type: String, required: true}
});