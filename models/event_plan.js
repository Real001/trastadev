var mongoose = require('mongoose');

module.exports = mongoose.model('Event_Plan',{
    event: {type: mongoose.Schema.Types.ObjectId, ref: 'events'},
    user:{type: mongoose.Schema.Types.ObjectId, ref: 'users'},
    status: {type: String}
})