var mongoose = require('mongoose');

module.exports = mongoose.model('Event',{
    title:{type: String, required: true},
    description: {type: String, required: true},
    location : {type: String, required: true},
    start_date: {type: Date, required: true},
    end_date: {type: Date, required: true},
    status: {type: String, required: true}
})