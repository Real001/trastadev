var mongoose = require('mongoose');

module.exports = mongoose.model('Plan',{
   deadline: {type: Date},
    appointmentDate: {type: Date},
    course: {type: mongoose.Schema.Types.ObjectId, ref: 'courses'},
    user:{type: mongoose.Schema.Types.ObjectId, ref: 'users'},
    started: {type: Date},
    passed: {type: Boolean, default: false},
    status: {
        type: String, required: true,
        enum:['open', 'inProgress', 'passed'],
        default: 'open'},
    result: {type: Number}
    //currentQuestion: {type: 'int', default: 0}
});