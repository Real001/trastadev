var mongoose = require('mongoose');

module.exports = mongoose.model('Admin',{
    username: {type: String, required: true},
    lastName: {type: String, required: true},
    firstName: {type: String, required: true},
    middleName: {type: String, required: true},
    Post: {type: String, required: true},
    password: {type: String},
});