var mongoose = require('mongoose');

module.exports = mongoose.model('Speciality',{
    name: {type:String, required: true}
});