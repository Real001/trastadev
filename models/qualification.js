var mongoose = require('mongoose');

module.exports = mongoose.model('Qualification',{
    name: {type:String, required: true}
});