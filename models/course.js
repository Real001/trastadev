var mongoose = require('mongoose');

module.exports = mongoose.model('Course',{
    title:{type: String, required: true},
    topic: {type: String, required: true},
    test: {type: Object},
    speciality: {type: String, required: true},
    qualification: {type: String, required: true},
    material: {type: Object}
});