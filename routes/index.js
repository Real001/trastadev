var express = require('express');
var router = express.Router();
var Admin = require('../models/admin');
var User = require('../models/user')
var updatedAdmin = require('../func/admin/updatedAdmin');
var updatedAdminPassword = require('../func/admin/updatedAdminPassword');
var createUser = require('../func/admin/createUser');
var OutputUser = require('../func/admin/OutputUser');
var DetailsUser = require('../func/admin/DetailsUser');
var updateUser = require('../func/admin/updateUser');
var DisplayPlan = require('../func/plan/Display');
var createSpeciality = require('../func/speciality/createSpeciality');
var DisplaySpeciality = require('../func/speciality/DisplaySpeciality');
var createQualification = require('../func/qualification/createQualification');
var DisplayQualification = require('../func/qualification/DisplayQualification');
var createQuestion = require('../func/question/createQuestion');
var CreateTest = require('../func/test/CreateTest');
var CreateCourse = require('../func/course/CreateCourse');
var DisplayCourse = require('../func/course/DisplayCourse');
var DisplayUserCourse = require('../func/course/DisplayUserCourse');
var DetailsCourse = require('../func/course/DetailsCourse');
var UpdateCourse = require('../func/course/UpdateCourse');
var CreatePlan = require('../func/plan/create');
var DetailsPlan = require('../func/plan/Details');
var StartTest = require('../func/test/StartTest');
var DisplayUserPlan = require('../func/admin/DisplayUserPlan');
var PlanUserDetails = require('../func/admin/PlanUserDetails');
var Event_create = require('../func/user/event/create');
var Event_display = require('../func/user/event/display');
var User = require('../models/user');


var isAuthenticated = function (req, res, next) {
  // if user is authenticated in the session, call the next() to call the next request handler
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated())
    return next();
  // if the user is not authenticated then redirect him to the login page
  res.redirect('/');
};

module.exports = function(passport){
  OutputUser();
  DisplaySpeciality();
  DisplayQualification();
  DisplayCourse();

  /* login admin page*/
  router.get('/admin', function(req, res){
    if (req.user){
      res.redirect('/users');
    } else {
      res.render('admin/index', {message: req.flash('message')});
    }});

  router.post('/admin', passport.authenticate('loginA', {
    successRedirect: '/users',
    failureRedirect: '/admin',
    failureFlash: true
  }));

  router.get('/users',isAuthenticated, function(req, res){
    OutputUser();
    /*User.find(function (err, users) {
      var context = {
        users: users.map(function (user) {
          return {
            lastName: user.lastName,
            firstName: user.firstName,
            middleName: user.middleName,
            post: user.post,
            speciality: user.speciality,
            qualification: user.qualification,
            email: user.username,
           _id: user._id
          }
        })
      };*/
      res.render('users/index', {
        'admin': req.user,
      'users':OutputUser.context.users})
    //})
      //res.render('users/index', context, {
        //'admin': req.user,

        //'users': OutputUser.users,
        //'qualification': OutputUser.qualification,
        //'speciality': OutputUser.speciality
    });
   // });

  router.get('/users/create', isAuthenticated, function(req, res){
    DisplayQualification();
    DisplaySpeciality();
    res.render('users/create',{admin:req.user, specialities: DisplaySpeciality.speciality, qualifications: DisplayQualification.qualification});
  });

  router.post('/users/create' , createUser, function(req, res){
    res.redirect('users');
  });

  router.get('/users/details/:id', isAuthenticated, function(req, res){
    var id = req.url;
    id = id.substr(15);
    DetailsUser(id)

      res.render('users/details', {admin: req.user, 'userinfo': DetailsUser.context.userinfo});

  });

  router.get('/users/update/:id', isAuthenticated, function(req, res){
    DisplayQualification();
    DisplaySpeciality();
    var id = req.url;
    id = id.substr(14);
    DetailsUser(id);
    res.render('users/update',{'admin':req.user, 'userinfo':DetailsUser.userinfo, 'qualifications': DisplayQualification.qualification, 'specialities': DisplaySpeciality.speciality});
  });

  router.post('/users/update/:id', updateUser, function(req, res){
    res.redirect('users');
  });

  router.get('/users/career/:id', isAuthenticated, function(req, res){
    var id = req.url;
    id = id.substr(14);
    DetailsUser(id);
    res.render('users/career', {'admin': req.user, 'userinfo':DetailsUser.userinfo})
  });

  router.get('/admin/signup', function(req, res){
    res.render('admin/register',{message:req.message});
  });

  router.post('/admin/signup', passport.authenticate('signup',{
    successRedirect: '/users',
    failureRedirect: '/admin/signup',
    failureFlash : true
  }));

  router.get('/course', isAuthenticated, function(req, res){
    DisplayCourse();
    res.render('course/index', {admin: req.user, 'courses': DisplayCourse.course});
  });

  router.get('/course/create', isAuthenticated, function(req, res){
    DisplayQualification();
    DisplaySpeciality();
    res.render('course/create', {'admin': req.user, 'specialities': DisplaySpeciality.speciality, 'qualifications': DisplayQualification.qualification});
  });

  router.post('/course/create', CreateCourse, function(req, res){
    res.redirect('/course');
  });

  router.get('/course/details/:id', isAuthenticated, function(req, res){
    var id = req.url;
    id = id.substr(16);
    DetailsCourse(id);
    res.render('course/details', {'admin': req.user, 'course': DetailsCourse.course})
  });

  router.get('/course/update/:id', isAuthenticated, function(req, res){
    var id = req.url;
    id = id.substr(15);
    DetailsCourse(id);
    DisplayQualification();
    DisplaySpeciality();
    res.render('course/update', {'admin': req.user, 'course': DetailsCourse.course, 'specialities': DisplaySpeciality.speciality, 'qualifications': DisplayQualification.qualification});
  });

  router.post('/course/update/:id',UpdateCourse, function(req, res){
    res.redirect('/course');
  });

  router.get('/plan/:id', function(req, res){
    var id = req.url;
    id = id.substr(6);
    DisplayUserPlan(id);
    DetailsUser(id)
    res.render('users/plan', {'user': id, admin: req.user, 'plan': DisplayUserPlan.plan, 'course': DisplayUserPlan.course, 'user': DetailsUser.userinfo});
  });

  router.get('/plan/details/:id', function (req, res){
    var id = req.url;
    id = id.substr(6);
    PlanUserDetails(id);
    res.render('user/plandetails', {'admin': req.user, 'user': id, 'plan':PlanUserDetails.plan, 'course': PlanUserDetails.course  })
  });

  router.get('/material/create', isAuthenticated, function(req, res){
    res.render('material/create', {admin: req.user});
  });

  router.get('/test/create', isAuthenticated, function(req, res){
    question = createQuestion.question;
    res.render('test/create', {admin: req.user, question: createQuestion.question});
  });

  router.post('/test/create', CreateTest, function(req,res){
    res.redirect('/material/create');
  });

  router.get('/question/create', isAuthenticated, function(req,res){
    res.render('question/create', {'admin': req.user});
  });

  router.post('/question/create', function(req, res){
    createQuestion(req, res);
   res.redirect('/test/create');
  });

  router.get('/speciality', isAuthenticated, function(req, res){
    DisplaySpeciality();
    res.render('speciality/index', {admin: req.user, 'specialities': DisplaySpeciality.speciality});
  });
  router.post('/speciality/create' , createSpeciality, function(req, res){
    res.redirect('speciality');
  });

  router.get('/qualification', isAuthenticated, function(req, res){
    DisplayQualification();
    res.render('qualification/index', {'admin': req.user, 'qualifications': DisplayQualification.qualification})
  });

  router.post('/qualification/create', createQualification, function(req, res){
    res.redirect('qualification');
  });

  router.get('/profile', isAuthenticated, function(req, res){
    res.render('profile/index', {admin:req.user});
  });

  router.post('/profile',updatedAdmin, function(req, res) {
    res.redirect('users');
  });

  router.get('/change',isAuthenticated, function(req, res){
    res.render('profile/change', {admin:req.user});
  });

  router.post('/change', updatedAdminPassword, function(req, res){
    res.redirect('users');
  });

  router.get('/admin/news/add', isAuthenticated, function(req, res){
    res.render('news/create',{admin:req.user});
  });

  router.get('/admin/news/view', isAuthenticated, function(req, res){
    res.render('news/view', {admin:req.user})
  })

  /*Logout*/
  router.get('/admin/logout', function(req, res){
    req.logout();
    res.redirect('/admin');
  });


  /*USER*/
  /* GET login user page. */
  router.get('/', function(req, res) {
    if (req.user){
      res.redirect('/user');
    } else {
      // Display the Login page with any flash message, if any
      res.render('user/login');
    }
  });

  router.post('/',passport.authenticate('loginA',{
        successRedirect: '/user',
        failureRedirect: '/',
        failureFlash: true
  }));

  router.get('/user', isAuthenticated, function(req, res){
    DisplayPlan(req, res);
    res.render('user/index', {'user': req.user, 'plan': DisplayPlan.plan, 'course': DisplayPlan.course, 'qualification':DisplayPlan.qualification, 'speciality':DisplayPlan.speciality});
  });

  router.get('/user/course', isAuthenticated, function(req, res){
    DisplayUserCourse(req, res);
    res.render('user/course', {'user': req.user, 'course': DisplayUserCourse.course, 'qualification': DisplayUserCourse.qualification, 'speciality': DisplayUserCourse.speciality});
  });

  router.get('/user/profile', isAuthenticated, function(req, res){
    res.render('user/profile', {user: req.user});
  });

  router.get('/user/course/details/:id', isAuthenticated, function(req, res){
    var id = req.url;
    id = id.substr(21);
    DetailsCourse(id);
    res.render('user/details', {'user': req.user, 'course': DetailsCourse.course})
  });

  router.get('/user/plan/create', isAuthenticated, function(req, res){
    DisplayUserCourse(req, res);
    res.render('user/plan', {'user' :req.user, 'course': DisplayUserCourse.course })
  });

  router.get('/user/plan/details/:id', isAuthenticated, function(req, res){
    var id = req.url;
    id = id.substr(19);
    DetailsPlan(id);
    res.render('user/plandetails', {'user': req.user, 'plan': DetailsPlan.plan, 'course': DetailsPlan.course})
  });

  router.post('/user/plan/create', CreatePlan, function(req, res){
    res.redirect('/user');
  });

  router.get('/user/test/start/:id', function(req, res){
    var id = req.url;
    id = id.substr(17)
    StartTest(id);
    res.render('user/test', {'user': req.user, 'course': StartTest.course});
  });

  router.get('/user/event/my', function(req, res){
    res.render('user/event/my', {'user': req.user});
  });

  router.get('/user/event/available', function(req, res){
    Event_display()
    res.render('user/event/available', {'user': req.user, 'event':Event_display.event});
  });

  router.get('/user/event/create', function (req, res) {
    res.render('user/event/create', {'user': req.user});
  });

  router.post('/user/event/create',Event_create, function(req, res) {
    res.redirect('user/event/my',{'user': req.user});
  });

  router.get('/user/logout', function(req, res){
    req.logout();
    res.redirect('/');
  });


  return router;
}